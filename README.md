# fortify-rules-decrypt

##  Description/描述
Fortify SCA rules decrypt tools

Fortify SCA 自带规则解密工具

##  Download/下载链接
https://bitbucket.org/codeplutos/fortify-rules-decrypt/downloads/

##  Usage/用法

```shell
java -jar fortify-rule-dencrypt.jar source_dir output_dir
```
解密程序会在source_dir中寻找`.bin`结尾的文件并解密，保存到output_dir中